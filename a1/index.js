// console.log("Hello World!");

// 1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the index.js file to the index.html file.
// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
const getCube = (x) => x**3;

let cube = getCube(3);


// // Template Literals
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of 3 is ${cube}.`);
// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.
let address = ["6754", "Ayala Ave", "Makati City", "Philippines"];

const [unitNum, streetName, cityAddress, Country] = address;

console.log(`I live in ${unitNum}, ${streetName}, ${cityAddress}, ${Country}, nice to meet you`)


// // Object Destructuring
// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const animal = {
	name: "Honey Badger",
	kingdom: "Animalia",
	genus: "Mellivora",
	binomialName: "Mellivora capensis"
};

const {name, kingdom, genus, binomialName} = animal;

console.log(`The animal ${name} is the only living species in the genus ${genus}, This animal belongs to the kingdom ${kingdom} and has a Binomial name of ${binomialName}.`)
// // Arrow Functions
// 9. Create an array of numbers.

const numbers = [0,1,2,3,4,5,6,7,8,9];

// // A
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach(numbers=>{console.log(numbers)})
// // B
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const iteration = 0;
const reduceNumber = numbers.reduce((previousValue, currentValue)=> previousValue + currentValue, iteration
);

console.log(reduceNumber);
// // Javascript Objects
// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const bigDog = new Dog("Gretel", 5, "German Shepherd");
console.log(bigDog);
const smallDog = new Dog("Munchkin", 3, "Chihuahua")
console.log(smallDog);
// 14. Create a git repository named S24.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of "Add s24 activity code".
// 16. Add the link in Boodle.

